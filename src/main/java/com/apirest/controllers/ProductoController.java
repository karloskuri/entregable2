package com.apirest.controllers;

import com.apirest.models.ProductoModel;
//import com.apirest.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.apirest.services.ProductoDBService;

import java.util.List;


@RestController
@RequestMapping("${url.base}")
public class ProductoController {
    @Autowired
    private ProductoDBService productoDBService;

    @GetMapping("")
    public String root() {
        return "Techu API REST v2.0.0 - Entregable02 - Anthonny Caro";
    }

    // GET todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoDBService.findAll();
    }

    //POST para crear un producto
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel newProduct) {
        productoDBService.save(newProduct);
        return newProduct;
    }

    // GET a un único producto por ID (instancia)
    @GetMapping("/productos/{id}")
    public ProductoModel getProductoById(@PathVariable String id){
        return productoDBService.myfindById(id);
    }

    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel productoToUpdate){
        productoDBService.save(productoToUpdate);
    }

    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel productoToDelete){
        return productoDBService.delete(productoToDelete);
    }

}