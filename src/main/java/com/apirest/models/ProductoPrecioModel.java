package com.apirest.models;

public class ProductoPrecioModel {
    private long id;
    private double precio;

    public ProductoPrecioModel() {
    }

    public ProductoPrecioModel(double precio) {
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
